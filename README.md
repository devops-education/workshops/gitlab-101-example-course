# GitLab 101 Example Course

This set of resources aims to be something that can be dropped into any appropriate course using GitLab. Whether it's for code management, project management, or data research, these lectures and resources, complete with example assignments, is free to use, copy, or distribute to other educators looking for ways to teach GitLab and DevOps in their courseload. 

## How To Use

Much of the useful work are in issues for this project. Homework and classwork will remain in issues. Links in this readme to the lectures are available in Google SLides format as well as Pdfs in the repository. 

## Lecture SlideDecks

- [Week 1 Lecture 1](https://docs.google.com/presentation/d/1GmdQ2oYRQzyqA3gKQy551aYs5sZSwPzRDg_ZtZ3qlAQ/edit?usp=sharing) What is DevOps?
- [Week 1 Lecture 2](https://docs.google.com/presentation/d/17M9Ny2jqToVVerNn5-oDQ0BNrh3P_WAg-4G2egHEuOc/edit?usp=sharing) The Web IDE
- [Week 2 Lecture 1](https://docs.google.com/presentation/d/1RAvWUyldpObzUox3vH3VlmLlgRJE3j5y0ns1PoYu5UA/edit?usp=sharing) People, Processes, Tools
- [Week 2 Lecture 2](https://docs.google.com/presentation/d/18YRrK0V4tWybEfYiM6egi7PLGNCs7HvY-2juUq-C4I0/edit?usp=sharing) What is Git?
- [Week 3 Lecture 1](https://docs.google.com/presentation/d/1r0M7RNnUYKTXHNZa-VctqZXxL7hOUyTBhrk7BRv5O70/edit?usp=sharing) Project Management with GitLab
- [Week 3 Lecture 2](https://docs.google.com/presentation/d/1OiWYTEO-8Z6-sssGv5YjeAg0RpnMIfmWmw_cAWk5hGY/edit?usp=sharing) Security and DevSecOps
- [Week 4 Lecture 1](https://docs.google.com/presentation/d/17E9-ukVCNYrqlXZJIoKm9y-HslViknoKLAIf03icc5w/edit?usp=sharing) CI/CD
- ~~Week 4 Lecture 2~~ No Lecture, in class work for CI/CD as it's a more complicated assignment. 
- [Week 5 Lecture 1](https://docs.google.com/presentation/d/1Q9uJe-ChIxDMmY7Rx9lzUBEfcRzjVNOQNejaeN9TEMw/edit?usp=sharing) Open Source
- [Week 5 Lecture 2](https://docs.google.com/presentation/d/1flxQlSdMmWiHI5tYl1LC89d9zvtvP298l-yYu_NelJI/edit#slide=id.p1) Monitoring and Observability

